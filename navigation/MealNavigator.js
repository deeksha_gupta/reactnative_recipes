import React from 'react';
import { Platform, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation'
import CategoriesScreen from '../screens/CategoriesScreen';
import CategoryMealsScreen from '../screens/CategoryMealsScreen';
import MealDetailsScreen from '../screens/MealDetailsScreen';
import Colors from '../constants/Colors';
import StartScreen from '../screens/StartScreen';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import FavoritesScreen from '../screens/FavoritesScreen';

const defaultStackNavOptions = {
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primary : 'white',
    },
    headerTitleStyle: {
        fontFamily: 'open-sans-bold'
      },
      headerBackTitleStyle: {
        fontFamily: 'open-sans'
      },
    headerTintColor: Platform.OS ==='android' ? 'white' : Colors.primary,
    headerTitle: 'A Screen'
};

const MealsNavigator = createStackNavigator({
    Start: StartScreen,
    Login: LoginScreen,
    Register: RegisterScreen,
    Categories: {
        screen: CategoriesScreen,
        initialRouteName: 'Meal Categories',
        navigationOptions: {
            headerStyle: {
                backgroundColor: Colors.primary
            },
            headerTintColor: 'white' 
        },
    },
    CategoryMeals: {
        screen: CategoryMealsScreen,
    },
    MealDetail: MealDetailsScreen,
},
{
    defaultNavigationOptions: defaultStackNavOptions 
});

const FavNavigator = createStackNavigator({
    Favorites: FavoritesScreen,
    MealDetail: MealDetailsScreen,
},
{
    defaultNavigationOptions: defaultStackNavOptions
});

const tabScreenConfig = {
    Meals: { 
        screen: MealsNavigator, 
        navigationOptions: {
        tabBarIcon: (tabInfo) => {
           return <Ionicons name='ios-restaurants' size={25} color={tabInfo.tintColor} />
        },
        tabBarColor: Colors.primary,
        tabBarLabel:
        Platform.OS === 'android' ? (
          <Text style={{ fontFamily: 'open-sans-bold' }}>Meals</Text>
        ) : (
          'Meals'
        )
    }},
    Favorites: { 
        screen: FavNavigator,
        navigationOptions: { 
         tabBarIcon: (tabInfo) => {
             return <Ionicons name='ios-star' size={25} color={tabInfo.tintColor} />
          },
         tabBarColor: Colors.primary,
         tabBarLabel:
        Platform.OS === 'android' ? (
          <Text style={{ fontFamily: 'open-sans-bold' }}>Favorites</Text>
        ) : (
          'Favorites'
        )  
     }
    }
 };

const MealsFavTabNavigator = Platform.OS === 'android' ? 
 createMaterialBottomTabNavigator(tabScreenConfig, {
    activeColor: 'white',
    shifting: true,
    barStyle: {
        backgroundColor: Colors.primary
      }
 }) :
 createBottomTabNavigator (tabScreenConfig, {
    tabBarOptions: {
        activeTintColor: Colors.accent,
        labelStyle: {
            fontFamily: 'open-sans'
        },
    }
});
  
export default createAppContainer(MealsFavTabNavigator);