import React from 'react';
import { CATEGORIES, MEALS } from '../data/dummy-data';
import Colors from '../constants/Colors';
import MealList from '../components/MealList';
import { Ionicons } from '@expo/vector-icons';
import MainButton from '../components/MainButton';

const CategoryMealsScreen = props => {

  const catId = props.navigation.getParam('categoryId');

  const displayedMeals = MEALS.filter(
    meal => meal.categoryIds.indexOf(catId) >= 0
  );

  return (
    <>
     <MealList listData={displayedMeals} navigation={props.navigation} />
     <MainButton 
       onPress={() => {}}>
       <Ionicons name="md-add" size={24} color="white" /> 
     </MainButton>
    </>
  );
};

CategoryMealsScreen.navigationOptions = navigationData => {
    const catId = navigationData.navigation.getParam('categoryId');
    
    const selectedCategory = CATEGORIES.find(cat => cat.id === catId);

    return {
        headerTitle: selectedCategory.title,
        headerStyle: {
            backgroundColor: Colors.primary
        },
    };
};

export default CategoryMealsScreen;