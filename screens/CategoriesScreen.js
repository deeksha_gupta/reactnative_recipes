import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { CATEGORIES } from '../data/dummy-data';
import CategoryGridTile from '../components/CategoryGridTile';
import { Ionicons } from '@expo/vector-icons';
import MainButton from '../components/MainButton';

const CategoriesScreen = props => {
    const renderGridItem = (itemData) => {
        return <CategoryGridTile 
                   title={itemData.item.title} 
                   Color={itemData.item.color}
                   onSelect = {() => {
                        props.navigation.navigate({ 
                            routeName: 'CategoryMeals',
                            params: {
                                categoryId: itemData.item.id
                            }
                        })
                    }}
                />;
    };
    return (
        <>
        <FlatList
           keyExtractor={(item, index) => item.id}
           data={CATEGORIES}
           renderItem={renderGridItem}
           numColumns={1}   
        />
        <MainButton 
           onPress={() => {}}>
           <Ionicons name="md-add" size={24} color="white" /> 
        </MainButton>
        </>
    );
};

CategoriesScreen.navigationOptions = navData => {
    return {
      headerTitle: 'Meal Categories',
    };
  };
  
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
});

export default CategoriesScreen;