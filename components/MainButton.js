import React from 'react';
import { View, Text , StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';

const MainButton = props => {
    return (
        <TouchableOpacity activeOpacity={0.6} onPress={props.onPress} style={styles.main}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>{props.children}</Text>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: Colors.primary,
        borderRadius: 50,
        marginBottom: 20,
        width:70,
        height:70,
        alignItems:'center',
        justifyContent:'center',
    },
    main: {
        alignItems:'flex-end',
        justifyContent:'flex-end',
        marginRight: 20,
        position: 'absolute',
        bottom:0,
        right:0,
    },
    buttonText: {
        color: 'white',
        fontSize: 12
    }
});

export default MainButton;